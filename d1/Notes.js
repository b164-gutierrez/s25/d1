// Aggregation in MongoDB

// Example Scenario

name: 'Jane',
enrollment: [

	{
		courseId: '1',
		status: 'ongoing'
	},
	{
	courseId: '2',
	status: 'completed'	
	}
]


name: 'Kelly',
enrollment: [

	{
		courseId: '3',
		status: 'completed'
	},
	{
	courseId: '2',
	status: 'drop'	
	}
]



// MongoDB Aggregation
// used to generate manipulated adata and perform operations to create filtered results that helps in analyzing data.


// using Aggregate Method
// $match - used to pass the documents that meet the specified condition (1st Stage) like find {criteria}.

/*
	syntax:
			{$match: {field: value} }
*/

// sample
db.fruits.aggregate([
    {$match: {onSale: true} }

])


// (2nd Stage)
// $group - used to group elements together and field-value pairs using the data from the grouped elements.

/*
	syntax:
			{$group: {field: value, fieldResult: 'valueResult'} } 
									or
			{$group: {_id: 'value', fieldResult: 'valueResult'} }
*/

//sample
db.fruits.aggregate([
    {$match: {onSale: true} }, 
    {$group: {_id: '$supplier_id', totalStocks: {$sum: '$stock'} }}

]) //sum of stocks





// the '$' sign symbol refer to a field name that is available in the documents that are being aggregated on

// $sum operator will total the values of all 'stocks' fields in the returned documents that are found using the '$match' criteria.




// Field Projection with Aggregation

// $project can be used when aggregating data to "INCLUDE" or "EXCLUDE" fields for the returned results.

// sample
db.fruits.aggregate([
    {$match: {onSale: true} }, 
    {$group: {_id: '$supplier_id', totalStocks: {$sum: '$stock'} }},
    {$project: {_id: 0}}

]) //id removed



// if we want to count all the documents
db.fruits.aggregate([
     
    {$group: {_id: null, myCount: {$sum: 1} }},
    {$project: {_id: 0}}

])

// if you specify an _id: value null, or any other constant value, the $group stage calculate accumulated values for all the input documents as a whole.



// Sorting Agrregated Results
// $sort - used to change the order of aggregated results.
// -1 (decrement) / 1 (increment)

/*
	syntax:
			{$sort: {field: -1 or 1}}
*/

// sample
db.fruits.aggregate([
    {$match: { onSale: true } },
    { $group: {_id: "$supplier_id", totalStocks: {$sum: "$stock"} } },
    {$sort: {totalStocks: -1}}
    ])



// Aggregating results based on Array Fields
// $unwind - deconstruct an array field from a collection/field with an array value to output a result for each element.

/*
	syntax:

*/

// sample
db.fruits.aggregate([
    {$unwind: '$origin'}
    ])

// the syntax will return results creating separate documents for each 

// sample
db.fruits.aggregate([
    {$unwind: '$origin'},
    {$group: {_id: '$origin', kinds: {$sum: 1}}}
    ])

// sample
db.fruits.aggregate([
    {$unwind: '$origin'},
    {$group: {_id: '$color', kinds: {$sum: 1}}}
    ])


// $count

// sample
db.scores.aggregate([
	{$match: {score: {$gt: 80} }},
        {$count: 'passing_scores'}
])

// {$count: <string>}

// <string> is the container